Kubernetes postconfigure
============

Ansible role that postoinfigure kubernetes

Requirements
------------

`helm` and `kubectl` with right configuration must be installed on the servers.
`pip` also must be present.

Role Variables
--------------


Dependencies
------------

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: kubernetes-postconfigure
```

TODO
----

- [ ] add more dashboards for infrastructure supervision
- [ ] configure kubernetes prometheus service endpoints
- [ ] create some alerts

License
-------

Apache 2.0
